module gitlab.com/unit410/threshold-ed25519

go 1.19

require (
	github.com/stretchr/testify v1.3.0
	gitlab.com/unit410/edwards25519 v0.0.0-20220725154547-61980033348e
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
